%tooling.fic  /* usinage, sous-usinage, style, smart, combo, vis scoop
Corte
2   1 5 0 2 1
Corte internal forced
22  1 5 0 2 0
Corte external forced
32  1 5 0 2 0
Marcado
1   1 8 1 2 1
Marcado internal forced
21  1 8 1 2 0
Marcado external forced
31  1 8 1 2 0
Punteado
3   1 7 1 2 1
Punteado internal forced
23  1 7 1 2 0
Punteado external forced
33  1 7 1 2 0
Cateye
4 1 3 0 2 1

100 3 1  1 0 0
Dimension
105 1 1  1 0 0
Starting point
205 1 10 1 0 0
Chamfer
305 1 7  1 0 0
Tapering
405 1 12 1 0 0
Sheet
100 1 1  1 1 0
Decoration
99  0 12 1 2 0
Construction
20  1 9  1 1 0
Rectangle notching
82  1 7  1 0 0
Circular notching
92  1 7  1 0 0
Recovery
12  1 5  1 0 0
Sequence
3  99 8  1 0 0
Worshop sequence
9   1 10 1 1 0
Lines
121 1 1  1 1 0
Hidden lines
120 1 9  1 1 0
Sheet support
130 1 9  1 1 0
Chute fiche
27 1 1  1 0 0
End of list
0 0 0 0 0 0
%prop.fic
EVACUA 15
1 1 3 1 0
1 1 5 1 0
1 1 5 1 0
1 1 5 1 0
1 1 5 1 0
1 1 5 1 0
1 1 5 1 0
1 1 5 1 0
1 1 5 1 0
1 1 5 1 0
1 1 5 1 0
1 1 5 1 0
1 1 5 1 0
1 1 5 1 0
1 1 5 1 0
CHANFR 5
1 1 3 1 0
1 1 3 1 0
1 1 3 1 0
1 1 3 1 0
1 1 3 1 0
DELARD 5
1 1 3 1 0
1 1 3 1 0
1 1 3 1 0
1 1 3 1 0
1 1 3 1 0
END
%
